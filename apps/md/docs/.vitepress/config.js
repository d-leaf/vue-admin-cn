const gitee = '<svg t="1665887540320" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1792" width="32" height="32"><path d="M512 1024C229.222 1024 0 794.778 0 512S229.222 0 512 0s512 229.222 512 512-229.222 512-512 512z m259.149-568.883h-290.74a25.293 25.293 0 0 0-25.292 25.293l-0.026 63.206c0 13.952 11.315 25.293 25.267 25.293h177.024c13.978 0 25.293 11.315 25.293 25.267v12.646a75.853 75.853 0 0 1-75.853 75.853h-240.23a25.293 25.293 0 0 1-25.267-25.293V417.203a75.853 75.853 0 0 1 75.827-75.853h353.946a25.293 25.293 0 0 0 25.267-25.292l0.077-63.207a25.293 25.293 0 0 0-25.268-25.293H417.152a189.62 189.62 0 0 0-189.62 189.645V771.15c0 13.977 11.316 25.293 25.294 25.293h372.94a170.65 170.65 0 0 0 170.65-170.65V480.384a25.293 25.293 0 0 0-25.293-25.267z" fill="#C71D23" p-id="1793"></path></svg>'
const domain = 'http://vue-admin.cn/'

module.exports = {
  base:'/docs/',
  title: "Vue-Admin 帮助中心",
  description: "只是一个管理后台",
  themeConfig: {
    siteTitle: "Vue-Admin",
    logo: "/logo.png",
    footer: {
      message: "MIT Licensed",
      copyright: "Copyright © 2022 Vue Admin",
    },
    sidebar: {
      "/guide": sidebarGuide(),
      "/about": sidebarAbout(),
    },

    nav: [
      { text: "首页", link: "/" },
      { text: "指南", link: "/guide/introduction/" },
      { text: "关于我们", link: "/about/" },
      {
        text: "在线演示",
        items: [
          { text: "Nest-Vue-Admin 版本", link: "http://nest.vue-admin.cn" },
          { text: "Express-Vue-Admin 版本", link: "http://express.vue-admin.cn" },
          // { text: "Koa2-Vue-Admin 版本", link: "/item-3" },
        ],
      },
    ],

    socialLinks: [
      {
        icon: {
          svg: gitee,
        },
        link: "https://gitee.com/jsfront",
      },
      { icon: "github", link: "https://github.com/jsfront" },
    ],
  },

};

function sidebarGuide() {
  return [
    {
      text: "入门",
      collapsible: true,
      items: [
        {
          text: "简介",
          link: "/guide/introduction/",
        },
      ],
    },
    {
      text: "快速开始",
      collapsible: true,
      items: [
        {
          text: "环境配置",
          link: "/guide/start/env/",
        },
        {
          text: "初始化",
          link: "/guide/start/init/",
        },
        {
          text: "swagger",
          link: "/guide/start/swagger/",
        },
      ],
    },
    {
      text: "部署指南",
      collapsible: true,
      items: [
        {
          text: "项目上线",
          link: "/guide/deployment/",
        },
        {
          text: "docker",
          link: "/guide/deployment/docker/",
        },
      ],
    },
  ];
}

function sidebarAbout() {
  return [
    {
      text: "关于我们",
      items: [
        {
          text: "我们是谁",
          link: "/about/",
        },
        {
          text: "如何找到我们",
          link: "/about/contact",
        },
      ],
    },
  ];
}
