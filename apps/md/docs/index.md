---
layout: home

hero:
  name: Vue-Admin
  text: Vue3平台解决方案
  tagline: 为基础业务提供多种模板以便于高效的实现业务需求
  image:
    src: /vue-banner.png
    alt: VitePress
  actions:
    - theme: brand
      text:  快速上手
      link: /guide/
    - theme: alt
      text: 文档
      link: /guide/

features:
  - title: 类型多样
    details: 包括各种后台实现
  - title: 风格多样
    details: 各种主题应有尽有
  - title: 代码多样
    details: 各种实现应有尽有
  - title: 思路多样
    details: 各种表现应有尽有
---
